# waterfall-exec
Executes an array of `Promise` one after another.
Can also listen to the `array` for new items.

## Install
```bash
$ yarn add waterfall-exec
```

## Usage
```javascript
const
	waterfallExec = require('waterfall-exec'),
	{Waterfall} = waterfallExec;

let
	jobs = [
		() => Promise.resolve(),
		() => Promise.resolve('some value'),
		someValue => Promise.resolve(someValue)
	],
	waterfall = new Waterfall();

waterfall.exec(jobs)
	.then(result => {
		console.log(result); // logs 'some value' into the console
	});
```

`Waterfall` uses standard `Promise` chain to synchronous execution of an array of asynchronous functions. Just pass an array of functions and `Waterfall` starts to execute them one by one.

### API

#### Waterfall.exec(jobs [, options])
**jobs** | `Array`
`Array` of `function` items. Each `function` should return either `Promise` or some data to be passed as resolved data.

**options** | `object` _optional_
- **waitForItems** | `boolean` _optional_
  causes main Promise is never resolved and Waterfall executes every new job added to the `jobs` array. Default is `false`.
- **waitTimeout** | `number` _optional_
  timeout for `waitForItems`. If `undefined`, there is no timeout and `waterfall` will tick forever. Default is `undefined`.
- **checkInterval** | `number` _optional_
  interval in miliseconds for `waitForItems`. Default is `1000`.
- **onNewPromise** | `function` _optional_
  function to be called when new job starts. Accepts the step `Promise` as the first argument.

#### Waterfall.stop()
Stops the execution and clears the `jobs` array. Also sets `waitForItems` to `false`, so if you want to resume the execution, you need to call `Waterfall.exec()` again.

## Testing
Tests are using [AVA library](https://github.com/avajs/ava)

```bash
$ yarn test                              // run all tests
$ yarn test -- -m 'Test title RegExp'    // run test with matching title
```
