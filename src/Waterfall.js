const
	assertUtils = require('assert-utils'),
	{assert} = assertUtils;

class Waterfall {
	constructor() {
		this.options = {};
	}

	/**
	 * @param  {function[]} jobs array of functions, each returning either Promise or result data
	 * @param  {Object} [options] options object
	 * @param  {boolean} [options.waitForItems] causes main Promise is never resolved and Waterfall executes every newly added job
	 * @param  {number} [options.waitTimeout] timeout for waitForItems, undefined means no timeout
	 * @param  {number} [options.checkInterval] interval for waitForItems in ms, default 1000
	 * @param  {function} [config.onNewPromise] is called when next step is being executed
	 * @return {Promise}
	 */
	exec(jobs, options) {
		assert.type(jobs, '[]');
		assert.type(options, {
			_required: false,
			waitForItems: '?boolean',
			waitTimeout: '?number',
			checkInterval: '?number',
			onNewPromise: '?function'
		});

		this.jobs = jobs;
		this.options = options || {};

		if (this.options.checkInterval === undefined) {
			this.options.checkInterval = 1000;
		}
		return this._doJob();
	}

	/**
	 * Stops execution and clears jobs array
	 */
	stop() {
		this.jobs = [];
		this.options.waitForItems = false;
	}

	/**
	 * Executes another step
	 * @param  {*} [result] Some result data returned by the step
	 * @return {Promise}
	 */
	_doJob(result) {
		let
			job, jobResult, promise;

		if (this.jobs.length) {
			console.debug('Waterfall: job found');

			job = this.jobs.shift();

			console.assert(typeof job === 'function');

			jobResult = job(result);

			promise = new Promise((resolve, reject) => {
				if (jobResult instanceof Promise) {
					jobResult
						.then(result => {
							return this._doJob(result);
						})
						.then(result => {
							resolve(result);
						})
						.catch(reason => {
							reject(reason);
						});
				}
				else {
					this._doJob(jobResult)
						.then(result => {
							resolve(result);
						})
						.catch(reason => {
							reject(reason);
						});
				}
			});

			if (this.options.onNewPromise) {
				this.options.onNewPromise(promise);
			}

			return promise;
		}
		else {
			if (this.options.waitForItems) {
				return new Promise((resolve, reject) => {
					this._waitForJobs()
						.then(() => {
							this._doJob()
								.then(result => {
									resolve(result);
								})
								.catch(reason => {
									reject(reason);
								});
						})
						.catch(reason => {
							reject(reason);
						});
				});
			}
			else {
				return Promise.resolve(result);
			}
		}
	}

	_waitForJobs() {
		return new Promise((resolve, reject) => {
			this.start = Date.now();
			this.checker = setInterval(() => {
				if (this.jobs.length || !this.options.waitForItems) {
					clearInterval(this.checker);
					resolve();
				}
				else if (this.options.waitTimeout && (this.start + this.options.waitTimeout < Date.now())) {
					clearInterval(this.checker);
					reject('Waterfall: timed out');
				}
			}, this.options.checkInterval);
		});
	}
}

exports.Waterfall = Waterfall;
