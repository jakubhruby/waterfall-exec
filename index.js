const
	{Waterfall} = require('./src/Waterfall.js');

exports = {
	Waterfall: Waterfall
};

// CommonJS
module.exports = exports;
